/*5) Dibuje en pantalla el siguiente triángulo rectángulo, utilizando bucles. 
El usuario ingresa un número que se tomará como el tamaño de los catetos. El siguiente ejemplo sería con cateto 5:
*
**
***
****
*****
*/
#include <stdio.h>
#include <conio.h>
void imprime_un_triangulo_segun_el_numero_de_entrada ();

int main (){
    int largo_de_los_catetos;

    printf ("\nIngrese el largo de los catetos:");
    scanf ("%d", &largo_de_los_catetos);
    imprime_un_triangulo_segun_el_numero_de_entrada (largo_de_los_catetos);
    getch ();
}

void imprime_un_triangulo_segun_el_numero_de_entrada (int numero_recivido)   //imprime en pantalla un triangulo empezando de izquierda a derecha segun el valorr recivido
{
    int largo_del_cateto, impresion_de_asteriscos, asteriscos_maximos = 1;

    for (largo_del_cateto = 1; largo_del_cateto <= numero_recivido; largo_del_cateto++)
    {  
        for (impresion_de_asteriscos = 1; impresion_de_asteriscos <= asteriscos_maximos; impresion_de_asteriscos++) 
        {
            printf ("*");
        }
        printf ("\n");
        asteriscos_maximos++;
    }
}