/*4) Los números "perfectos" son aquellos que la suma de los divisores (excluyendo el número mismo) es igual al número.
Por ejemplo el 6 es un "número perfecto" porque 1 + 2 + 3 = 6.
Encuentre y muestre en pantalla 2 números perfectos más.*/
#include <stdio.h>
#include <conio.h>
#define MAXIMOS_DE_PERFECTOS_DESEADOS 2
void numeros_de_7_a_infinito ();
int devuelve_1_si_es_un_numero_perfectos ();

int main (){
    numeros_de_7_a_infinito ();
    getch ();
}

void numeros_de_7_a_infinito (){        //Encuentra los números perfectos deseados
    int numero_a_comprobar = 7;
    int contador_de_perfectos = 0;

    while ( 1 )                          
    {
        if (devuelve_1_si_es_un_numero_perfectos (numero_a_comprobar) == 1) 
        {
            printf ("\n%d es un numero perfecto\n", numero_a_comprobar);
            contador_de_perfectos = contador_de_perfectos + devuelve_1_si_es_un_numero_perfectos (numero_a_comprobar);
        }
        numero_a_comprobar++;
        if (contador_de_perfectos == MAXIMOS_DE_PERFECTOS_DESEADOS) break;
    }
}

int devuelve_1_si_es_un_numero_perfectos (int numero_recivido){      /*comprueba si el numero recivido es perfecto perfecto */
    int numero_por_el_que_divide;
    int suma_de_los_divisores = 0;

    for (numero_por_el_que_divide = 1; numero_recivido > numero_por_el_que_divide; numero_por_el_que_divide++)     
    {
        if (numero_recivido % numero_por_el_que_divide == 0) suma_de_los_divisores = suma_de_los_divisores + numero_por_el_que_divide;
    }
    if (suma_de_los_divisores == numero_recivido)   /*si es perfecto entra*/
    {
        return 1;
    }
    else return 0;
}
