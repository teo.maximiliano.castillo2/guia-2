/*4) Los números "perfectos" son aquellos que la suma de los divisores (excluyendo el número mismo) es igual al número.
Por ejemplo el 6 es un "número perfecto" porque 1 + 2 + 3 = 6.
Encuentre y muestre en pantalla 2 números perfectos más.*/
#include <stdio.h>
#include <conio.h>
#define MAXIMOS_DE_PERFECTOS_DESEADOS 2
void buscador_de_perfectos ();
int confirma_si_es_perfecto ();

int main (){
    buscador_de_perfectos ();
    getch ();
}

void buscador_de_perfectos (){ //Encuentra los números perfectos deseados
    int numero_a_comprobar_si_es_perfecto = 7;
    int contador = 0;
    int es_perfecto;
    
    while ( contador < MAXIMOS_DE_PERFECTOS_DESEADOS )                          
    {
        contador = contador + confirma_si_es_perfecto (numero_a_comprobar_si_es_perfecto);
        es_perfecto = confirma_si_es_perfecto (numero_a_comprobar_si_es_perfecto);
        numero_a_comprobar_si_es_perfecto++;
        if (es_perfecto != 0)
        { 
            printf ("\n%d es un numero perfecto\n",numero_a_comprobar_si_es_perfecto);
        }
    }
}

int confirma_si_es_perfecto (int perfecto){ /*confirma si es un numero perfectoss*/
    int divisores;
    int suma_de_divisores = 0;

    for (divisores = 1; perfecto > divisores; divisores++)     
    {
        if (perfecto % divisores == 0) suma_de_divisores = divisores + suma_de_divisores;
    }
    if (suma_de_divisores == perfecto) return 1;
        
    else return 0;
}