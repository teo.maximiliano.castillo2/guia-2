/*3) Utilizando la función anterior, arme un programa que muestre los números primos entre 2 y 100.*/
#include <stdio.h>
#include <conio.h>
int devuelve_1_si_es_primo();

int main (){
    int numero;
    int devolucion_de_variable;
    
    for (numero = 2; numero < 101; numero++)
    {
        devolucion_de_variable = devuelve_1_si_es_primo (numero);
        if (devolucion_de_variable != 0) printf ("\n%d", numero);
    }
    getch ();
}

int devuelve_1_si_es_primo (int recivido){   //Comprobacion si es primo
    int divisores;
    int contador_de_divisores = 0;

    for (divisores = 1; divisores <= recivido; divisores++) 
    {
        if (recivido % divisores == 0) contador_de_divisores++;
    }
    if (contador_de_divisores == 2) return 1;
        
    else return 0;
}