#include <stdio.h> 
#include <conio.h>
#define MAXIMOS_DE_PERFECTOS_DESEADOS 2
int opciones();
void ListadoDeOpciones();
int selecciones ();
void opcion1 ();
void opcion2 ();
int opcion3 ();
void opcion4 ();                  
void opcion5 ();
void opcion6 ();
void opcion7 ();
//Funciones que completan las opciones
int devuelve_1_si_es_primo();
void numeros_de_7_a_infinito ();
int devuelve_1_si_es_un_numero_perfectos ();
void imprime_un_triangulo_de_izquierda_a_derecha ();
void dibuja_triangulo_de_derecha_a_izquierda ();
void techo_y_piso_del_cuadrado ();
void laterales_del_cuadrado ();

int main() {
    while ( 1 )
    {
    ListadoDeOpciones();
    if (opciones() == 0) break;
    }
    getch ();
}

void ListadoDeOpciones(){
    printf ("\n\nEl programa puede realizar las siguientes opciones:");
    printf ("\n1) Halla los divisores del numero deseado.");
    printf ("\n2) Funcion que regresa 1 si es un numero primo o 0 si no lo es.");
    printf ("\n3) Muestra los numeros primos entre 2 y 100.");
    printf ("\n4) Encuentra %d numeros perfectos ademas de 6.", MAXIMOS_DE_PERFECTOS_DESEADOS);
    printf ("\n5) Hacer un triangulo de izquierda a derecha del largo deseado.");
    printf ("\n6) Hacer un triangulo de derecha a izquierda del largo deseado.");
    printf ("\n7) Hacer un cuadrado del largo deseado.");
    printf ("\n8) Si se ingresa el 8 se cerrara el programa.");
}

int opciones (){
    int valordelswitch;
    int retorno_de_variable;

    printf ("\n\nSeleccione una opcion:");
    scanf ("%d",&valordelswitch);
    retorno_de_variable = selecciones (valordelswitch);
    if (retorno_de_variable == 0) return 0; 
    getch ();
}

int selecciones (int valorRecivido){
    switch (valorRecivido)
    {
        case 1:
            opcion1 ();
            break;
        case 2:
            opcion2 ();
            break;
        case 3:
            opcion3 ();
            break;
        case 4:
             opcion4 ();
            break;
        case 5:
            opcion5 ();
            break;
        case 6:
            opcion6 ();
            break;
        case 7:
            opcion7();
            break;
        case 8:
        return 0;
            break;
        }
}

void opcion1 (){        //Encuentra los divisores del numero indicado
    int numero;
    int divisores;

    printf ("\nIngrese un numero para saber sus divisores: ");
    scanf ("%d",&numero);
    for (divisores = 1; numero >= divisores; divisores++)
    {
        if (numero % divisores == 0) printf ("\n%d es divisible por: %d", numero, divisores);
    }
}

void opcion2 (){        //Llama a la funcion primo para y le ingresa un valor
    int numero;
    int devolucion_de_variable;
    
    for (numero = 2; numero < 101; numero++)
    {
        devolucion_de_variable = devuelve_1_si_es_primo (numero);
        if (devolucion_de_variable != 0) printf ("\n%d", numero);
    }
} 

int opcion3 (){        //Encuentra los primos entre los numeros indicados
    int numero;
    int devolucion_de_variable;
    
    for (numero = 2; numero < 101; numero++)
    {
        devolucion_de_variable = devuelve_1_si_es_primo (numero);
        if (devolucion_de_variable != 0) printf ("\n%d", numero);
    }
    return 1;
}

void opcion4 (){        //Encuentra 2 números perfectos ademas de 6
    numeros_de_7_a_infinito ();
}

void opcion5 (){        //Hace un triangulo de izquierda a derecha del tamaño deseado
    int largo_de_los_catetos;

    printf ("\nIngrese el largo de los catetos:");
    scanf ("%d", &largo_de_los_catetos);
    imprime_un_triangulo_de_izquierda_a_derecha (largo_de_los_catetos);
}

void opcion6 (){        //Hace un triangulo de derecha a izquierda del tamaño deseado
    int largo_del_cateto;

    printf ("\nIngrese la medida de los catetos:");
    scanf ("%d", &largo_del_cateto);
    dibuja_triangulo_de_derecha_a_izquierda (largo_del_cateto);
}

void opcion7 (){        //Hace un cuadrado del tamaño deseado
    int largo_de_los_catetos;

    printf ("\nIngrese la medida de los lados del cuadrado:");
    scanf ("%d", &largo_de_los_catetos);
    if (largo_de_los_catetos >= 4 && largo_de_los_catetos <= 10)
    {
        printf ("┌");
        techo_y_piso_del_cuadrado (largo_de_los_catetos);
        printf ("┐\n");
        laterales_del_cuadrado (largo_de_los_catetos);
        printf ("└");
        techo_y_piso_del_cuadrado (largo_de_los_catetos);
        printf ("┘");
    }
}

int devuelve_1_si_es_primo (int recivido){                                             /*Comprueba si es primo*/
    int divisores;
    int contador_de_divisores = 0;

    for (divisores = 1; recivido >= divisores; divisores++)
    {
        if (recivido % divisores == 0)
            contador_de_divisores++;
    }
    if (contador_de_divisores == 2) return 1;

    else return 0;
}

void numeros_de_7_a_infinito (){                                      /*va de 7 a infinito*/
    int numero_a_comprobar = 7;
    int contador_de_perfectos = 0;

    while ( 1 )                          
    {
        if (devuelve_1_si_es_un_numero_perfectos (numero_a_comprobar) == 1) 
        {
            printf ("\n%d es un numero perfecto\n", numero_a_comprobar);
            contador_de_perfectos = contador_de_perfectos + devuelve_1_si_es_un_numero_perfectos (numero_a_comprobar);
        }
        numero_a_comprobar++;
        if (contador_de_perfectos == MAXIMOS_DE_PERFECTOS_DESEADOS) break;
    }
}

int devuelve_1_si_es_un_numero_perfectos (int numero_recivido){       /*comprueba si el numero recivido es perfecto perfecto */
    int numero_por_el_que_divide;
    int suma_de_los_divisores = 0;

    for (numero_por_el_que_divide = 1; numero_recivido > numero_por_el_que_divide; numero_por_el_que_divide++)     
    {
        if (numero_recivido % numero_por_el_que_divide == 0) suma_de_los_divisores = suma_de_los_divisores + numero_por_el_que_divide;
    }
    if (suma_de_los_divisores == numero_recivido)   /*si es perfecto entra*/
    {
        return 1;
    }
    else return 0;
}

void imprime_un_triangulo_de_izquierda_a_derecha (int numero_recivido)/*imprime en pantalla un triangulo empezando de izquierda a derecha segun el valorr recivido*/
{
    int largo_del_cateto, impresion_de_asteriscos, asteriscos_maximos = 1;

    for (largo_del_cateto = 1; largo_del_cateto <= numero_recivido; largo_del_cateto++)
    {  
        for (impresion_de_asteriscos = 1; impresion_de_asteriscos <= asteriscos_maximos; impresion_de_asteriscos++) 
        {
            printf ("*");
        }
        printf ("\n");
        asteriscos_maximos++;
    }
}

void dibuja_triangulo_de_derecha_a_izquierda (int numero_recivido){   /*imprime un triangulo el valorr recivido*/
    int largo_del_cateto;
    int espacios;
    int numero_maximo_de_asteriscos = numero_recivido;

    for (largo_del_cateto = 1; largo_del_cateto <= numero_recivido; largo_del_cateto++)
    {  
        for (espacios = 1; espacios <= numero_maximo_de_asteriscos; espacios++) 
        {
            if (numero_maximo_de_asteriscos == espacios) 
            {
                printf ("*");
                while (espacios < numero_recivido)
                {
                    printf ("*");
                    espacios++;
                }
            }
            else printf (" ");
        }
        printf ("\n");
        numero_maximo_de_asteriscos--;
    }
}

void techo_y_piso_del_cuadrado (int largo_del_techo_o_piso){          /*genera el lado superior e inferior del cuadrado*/
    int largo_del_lado = 2;

    while (largo_del_lado < largo_del_techo_o_piso)
    {
        printf ("-");
        largo_del_lado++;
    }
}

void laterales_del_cuadrado (int largo_maximo_del_lateral){           /*genera los laterales del cuadrado*/
    int largo_del_lado;
    int limite_del_lado = largo_maximo_del_lateral;
    int espacio;

    for (largo_del_lado = 2; largo_del_lado < largo_maximo_del_lateral; largo_del_lado++)
    {
        printf ("|");
        for (espacio = 1; espacio <= largo_maximo_del_lateral; espacio++)
        {
            if (espacio == limite_del_lado)
            {
                printf ("|\n");
            }
            else printf (" ");
        }
    }
}
