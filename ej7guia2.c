/*7) El usuario ingresará un número entero entre 4 y 10 inclusives. 
Se pide dibujar en pantalla un cuadrado de ese tamaño utilizando los caracteres ┌ ┐ - | └ ┘
Si el usuario ingresa 4, se dibujará:

┌--┐
|  |
|  |
└--┘

Si ingresara 6, por ejemplo, sería:

┌----┐
|    |
|    |
|    |
|    |
└----┘
*/
#include <stdio.h>
#include <conio.h>
void techo_y_piso_del_cuadrado ();
void laterales_del_cuadrado ();

int main (){
    int largo_de_los_catetos;

    printf ("\nIngrese la medida de los lados del cuadrado:");
    scanf ("%d", &largo_de_los_catetos);
    if (largo_de_los_catetos >= 4 && largo_de_los_catetos <= 10)
    {
        printf ("┌");
        techo_y_piso_del_cuadrado (largo_de_los_catetos);
        printf ("┐\n");
        laterales_del_cuadrado (largo_de_los_catetos);
        printf ("└");
        techo_y_piso_del_cuadrado (largo_de_los_catetos);
        printf ("┘");
    }
    else return (0);
    getch ();
 }

void techo_y_piso_del_cuadrado (int largo_del_techo_o_piso){        //genera el lado superior e inferior
    int largo_del_lado = 2;

    while (largo_del_lado < largo_del_techo_o_piso)
    {
        printf ("-");
        largo_del_lado++;
    }
}

void laterales_del_cuadrado (int largo_maximo_del_lateral){       //genera los laterales
    int largo_del_lado;
    int limite_del_lado = largo_maximo_del_lateral;
    int espacio;

    for (largo_del_lado = 2; largo_del_lado < largo_maximo_del_lateral; largo_del_lado++)
    {
        printf ("|");
        for (espacio = 1; espacio <= largo_maximo_del_lateral; espacio++)
        {
            if (espacio == limite_del_lado)
            {
                printf ("|\n");
            }
            else printf (" ");
        }
    }
}
