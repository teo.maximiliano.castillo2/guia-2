/*6) Dibuje el mismo triángulo pero al revés. El siguiente ejemplo sería con cateto 4:

   *
  **
 ***
****
*/
#include <stdio.h>
#include <conio.h>
void dibuja_triangulo_de_derecha_a_izquierda ();

int main (){
    int largo_del_cateto;

    printf ("\nIngrese la medida de los catetos:");
    scanf ("%d", &largo_del_cateto);
    dibuja_triangulo_de_derecha_a_izquierda (largo_del_cateto);
    getch ();
}

void dibuja_triangulo_de_derecha_a_izquierda (int numero_recivido){   //imprime en pantalla un triangulo empezando de derecha a izquierda segun el valorr recivido
    int largo_del_cateto;
    int espacios;
    int numero_maximo_de_asteriscos = numero_recivido;

    for (largo_del_cateto = 1; largo_del_cateto <= numero_recivido; largo_del_cateto++)
    {  
        for (espacios = 1; espacios <= numero_maximo_de_asteriscos; espacios++) 
        {
            if (numero_maximo_de_asteriscos == espacios) 
            {
                printf ("*");
                while (espacios < numero_recivido)
                {
                    printf ("*");
                    espacios++;
                }
            }
            else printf (" ");
        }
        printf ("\n");
        numero_maximo_de_asteriscos--;
    }
}