/*1) El usuario ingresará un entero positivo. Imprima en pantalla todos sus divisores.*/
#include <stdio.h>
#include <conio.h>

int main (){
    int numero_ingresado;
    int numero_por_el_que_se_divide;

    printf ("\nIngrese un numero para saber sus divisores: ");
    scanf ("%d",&numero_ingresado);
    for (numero_por_el_que_se_divide = 1; numero_ingresado >= numero_por_el_que_se_divide; numero_por_el_que_se_divide++)
    {
        if (numero_ingresado % numero_por_el_que_se_divide == 0)
           printf ("\n%d es divisible por: %d", numero_ingresado, numero_por_el_que_se_divide);
    }
    getch ();
}