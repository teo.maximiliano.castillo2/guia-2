/*2) Construya una función llamada "primo" que reciba un entero positivo y devuelva un 1 si es primo, o un 0 si no lo es.
Un número positivo primo solamente tiene 2 divisores: el 1 y si mismo.
Llame desde el main a esta función para probar que funciona correctamente.*/
#include <stdio.h>
#include <conio.h>
int devuelve_1_si_es_primo();

int main (){
    int numero_ingresado;
    int devolucion;

    printf ("\nIngrese un numero para saber si es primo: ");
    scanf ("%d", &numero_ingresado);
    devolucion = devuelve_1_si_es_primo (numero_ingresado);
    getch ();
}

int devuelve_1_si_es_primo (int recivido){
    int divisores;
    int contador_de_divisores = 0;

    for (divisores = 1; divisores <= recivido; divisores++)
    {
        if (recivido % divisores == 0) contador_de_divisores++;
    }
    if (contador_de_divisores == 2) return 1;

    else return 0;
}